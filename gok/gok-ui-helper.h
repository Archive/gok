/*
 * gok-ui-helper.h
 *
 * Copyright 2009 Gerd Kohlberger <gerdko gmail com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOK_UI_HELPER_H__
#define __GOK_UI_HELPER_H__

#include <gtk/gtk.h>

/* assumes a global GtkBuilder object named 'builder' */
#define OID(n) (gok_ui_helper_get_object (builder, (n), __FILE__, __LINE__, G_STRFUNC))
#define WID(n) (GTK_WIDGET (OID ((n))))

G_BEGIN_DECLS

GtkBuilder * gok_ui_helper_load_file  (const gchar *file);

GObject *    gok_ui_helper_get_object (GtkBuilder  *builder,
				       const gchar *name,
				       const gchar *file,
				       gint         line,
				       const gchar *func);

G_END_DECLS

#endif /* __GOK_UI_HELPER_H__ */
