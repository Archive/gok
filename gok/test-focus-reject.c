/*
* test-focus-reject.c
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
GdkFilterReturn
gok_discard_focus_filter (GdkXEvent *xevent,
			  GdkEvent  *event,
			  gpointer  data)
{
  XEvent *xev = (XEvent *)xevent;	
  GList *list, *l;
  gint n_mapped;
  GtkWindow *window;
  gboolean has_focus;

  if (xev->xany.type == ClientMessage &&
      (Atom) xev->xclient.data.l[0] == gdk_x11_atom_to_xatom (
	      gdk_atom_intern ("WM_TAKE_FOCUS", False)))
    {
      return GDK_FILTER_REMOVE;
    }
  else
    {
      return GDK_FILTER_CONTINUE;
    }
}

void
on_window_realize                     (GtkWidget       *widget, 
				       gpointer        user_data)
{
  XWMHints wm_hints;
  Atom wm_window_protocols[3], atom_type_dock[1], atom_window_type;
  static gboolean initialized = FALSE;
  
  if (!initialized)
    {
      wm_window_protocols[0] = gdk_x11_get_xatom_by_name ("WM_DELETE_WINDOW");
      wm_window_protocols[1] = gdk_x11_get_xatom_by_name ("_NET_WM_PING");
      wm_window_protocols[2] = gdk_x11_get_xatom_by_name ("WM_TAKE_FOCUS");
    }
  
  wm_hints.flags = InputHint;
  wm_hints.input = False;
  
  gdk_error_trap_push ();

  XSetWMHints (GDK_WINDOW_XDISPLAY (widget->window),
	       GDK_WINDOW_XWINDOW (widget->window), &wm_hints);
  
  XSetWMProtocols (GDK_WINDOW_XDISPLAY (widget->window),
		   GDK_WINDOW_XWINDOW (widget->window), wm_window_protocols, 3);
  gdk_error_trap_pop ();
  
  if (widget->window) {
    gdk_window_set_decorations (widget->window, GDK_DECOR_ALL | 
				GDK_DECOR_MINIMIZE | GDK_DECOR_MAXIMIZE);
    gdk_window_set_functions (widget->window, GDK_FUNC_MOVE | GDK_FUNC_RESIZE);
  }

  gdk_window_add_filter (widget->window,
			 gok_discard_focus_filter,
			 NULL);

}


gint main (gint argc, gchar *argv[])
{
  GtkWidget *window;
  gtk_init (&argc, &argv);
  window = g_object_connect (gtk_widget_new (gtk_window_get_type (),
				"can_focus", FALSE,
				"type", GTK_WINDOW_TOPLEVEL,
				"window-position", GTK_WIN_POS_CENTER,
				"title","reject focus",
				"allow_grow", TRUE,
				"allow_shrink", TRUE,
				"border_width", 0,
				NULL),
			     "signal::realize", on_window_realize, NULL,
			     NULL);
  
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 100);
  
  gtk_widget_show (window);
  gtk_main ();
  return 0;
}

