/* gok-settings-dialog.c
*
* Copyright 2002 Sun Microsystems, Inc.,
* Copyright 2002 University Of Toronto
* Copyright 2009 Gerd Kohlberger <gerdko gmail com>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the
* Free Software Foundation, Inc., 59 Temple Place - Suite 330,
* Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include "gok-settings-dialog.h"
#include "gok-page-keysizespace.h"
#include "gok-page-actions.h"
#include "gok-page-feedbacks.h"
#include "gok-page-accessmethod.h"
#include "gok-page-wordcomplete.h"
#include "gok-page-keyboard.h"
#include "gok-page-accessmethod.h"
#include "gok-data.h"
#include "gok-log.h"
#include "main.h"
#include "gok-gconf-keys.h"
#include "gok-gconf.h"

/* pointer to the settings dialog window */
static GtkWidget* m_pWindowSettings;

/* pointer to the settings dialog xml data type */
GtkBuilder *builder;

static gboolean is_locked;

/**
* gok_settingsdialog_open
* 
* Creates the GOK settings dialog.
*
* @bShow: If TRUE the settings dialog will be shown. If FALSE the dialog will be
* created but not shown.
*
* returns: TRUE if the settings dialog could be created, FALSE if not.
**/
gboolean gok_settingsdialog_open (gboolean bShow)
{
	builder = gok_ui_helper_load_file ("gok-settingsdialog.ui");
	g_assert (builder != NULL);

	m_pWindowSettings = WID ("window");

	/* don't read gconf here */
	is_locked = FALSE;

	/* initialize all the pages */
	gok_settings_page_keysizespace_initialize ();
	gok_page_accessmethod_initialize ();
	gok_page_keyboard_initialize ();
	gok_page_actions_initialize ();
	gok_page_feedbacks_initialize ();
	gok_page_wordcomplete_initialize ();
	
	/* create a backup copy of all the settings */
	gok_data_backup_settings();
			
	if (bShow == TRUE)
	{
		gok_settingsdialog_show ();
	}

	return TRUE;
}

/**
* gok_settingsdialog_show
* 
* Displays the GOK settings dialog.
*
* returns: TRUE if the settings dialog was shown, FALSE if not.
**/
gboolean gok_settingsdialog_show()
{
	gboolean bFirstShowing;
	GtkWidget *label;
	GtkWidget *notebook;
	gboolean was_locked;

	g_assert (m_pWindowSettings != NULL);

	was_locked = is_locked;
	gok_gconf_get_bool (gconf_client_get_default(),
		GOK_GCONF_PREFS_LOCKED, &is_locked);
	
	if (was_locked != is_locked) {
		if (!is_locked) {
			gtk_widget_show (WID ("ActionsTab"));
			gtk_widget_show (WID ("AccessMethodsTab"));
		}
		else {
			gtk_widget_hide (WID ("ActionsTab"));
			gtk_widget_hide (WID ("AccessMethodsTab"));
		}
	}

	bFirstShowing = !gtk_widget_is_drawable (m_pWindowSettings);

	gtk_widget_show (m_pWindowSettings);

	/* if this is the first time we're showing the dialog then redisplay
	  the key size/spacing page so the example keys are centered */
	if (bFirstShowing == TRUE)
	{
		gok_settings_page_keysizespace_refresh();
	}
	
	return TRUE;
}

/**
* gok_settingsdialog_hide
* 
* Hides the GOK settings dialog.
**/
void gok_settingsdialog_hide()
{
	g_assert (m_pWindowSettings != NULL);

 	gtk_widget_hide (m_pWindowSettings);
}

/**
* gok_settingsdialog_get_window
*
* returns: A pointer to the settings dialog window.
**/
GtkWidget* gok_settingsdialog_get_window ()
{
 	return m_pWindowSettings;
}

/**
* gok_settingsdialog_close
* 
* Destroys the GOK settings dialog.
*
**/
void gok_settingsdialog_close()
{
	gok_log_enter();
	if (m_pWindowSettings != NULL)
	{
		gok_page_accessmethod_close();
		gtk_widget_destroy (m_pWindowSettings);
		g_object_unref (builder);
	}
	else
	{
		g_warning("The settings dialog doesn't exist in close function!");
	}
	gok_log_leave();
}

/**
* on_button_ok
* @pButton: Pointer to the button that was clicked.
* @user_data: Any user data associated with the button.
* 
* The OK button has been clicked. Apply any changes from the settings and
* hide the settings dialog.
**/
void on_button_ok (GtkButton* button, gpointer user_data)
{
	/* apply the current settings */
	on_button_try (NULL, NULL);
	
	/* copy the current settings to the backup */
	gok_settingsdialog_backup_settings();
	
	/* hide the settings dialog */
	gok_settingsdialog_hide();
}

/**
* on_button_try
* @pButton: Pointer to the button that was clicked.
* @user_data: Any user data associated with the button.
* 
* The TRY button has been clicked. Apply any changes from the settings but
* don't hide the dialog
**/
void on_button_try (GtkButton* button, gpointer user_data)
{
	gboolean bDataChanged;

	bDataChanged = FALSE;
	
	/* use the current settings */
	if (gok_settings_page_keysizespace_apply() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_actions_apply() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_keyboard_apply() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_feedbacks_apply() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_accessmethod_apply() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_wordcomplete_apply() == TRUE)
	{
		bDataChanged = TRUE;
	}
	
	/* if any settings have changed then refresh the keyboard */
	if (bDataChanged == TRUE)
	{
		gok_main_display_scan_reset();

		/* and post a warning if the new settings use corepointer */		
		gok_main_warn_corepointer (FALSE, TRUE, FALSE);
	}
}

/**
* on_button_revert
* @pButton: Pointer to the button that was clicked.
* @user_data: Any user data associated with the button.
* 
* The REVERT button has been clicked. Returns the settings to the way they were
* and update the keyboard.
**/
void on_button_revert (GtkButton* button, gpointer user_data)
{
	gboolean bDataChanged;

	bDataChanged = FALSE;
	
	if (gok_data_restore_settings() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_settings_page_keysizespace_revert() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_keyboard_revert() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_accessmethod_revert() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_wordcomplete_revert() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_actions_revert() == TRUE)
	{
		bDataChanged = TRUE;
	}
	if (gok_page_feedbacks_revert() == TRUE)
	{
		bDataChanged = TRUE;
	}

	/* if any setting has changed then refresh the keyboard and dialog controls */
	if (bDataChanged == TRUE)
	{
		gok_main_display_scan_reset();
	}
}

/**
* on_button_cancel
* @pButton: Pointer to the button that was clicked.
* @user_data: Any user data associated with the button.
* 
* The CANCEL button has been clicked. Return settings to the way they were and
* update the keyboard and hide the settings dialog.
**/
void on_button_cancel (GtkButton* pButton, gpointer user_data)
{
	/* revert to the original settings */
	on_button_revert(NULL, NULL);

	/* hide the settings dialog */
	gok_settingsdialog_hide();
}

/**
* on_button_help
* @pButton: Pointer to the button that was clicked.
* @user_data: Any user data associated with the button.
*
* The HELP button has been clicked.
**/
void on_button_help (GtkButton* pButton, gpointer user_data)
{
	const char* help_link_id = NULL;

	switch (gtk_notebook_get_current_page (GTK_NOTEBOOK (OID ("notebook1"))))
	{
		case 0:
			help_link_id = "gok-prefs-appearance";
			break;
			
		case 1:
			help_link_id = "gok-prefs-keyboards";
			break;
			
		case 2:
			help_link_id = "gok-prefs-actions";
			break;
			
		case 3:
			help_link_id = "gok-prefs-feedback";
			break;

		case 4:
			help_link_id = "gok-prefs-accessmethods";
			break;
			
		case 5:
			help_link_id = "gok-prefs-prediction";
			break;
			
		default:
			break;
	}

	gok_main_display_help ("gok", help_link_id);
}

/**
* gok_settingsdialog_refresh
* 
* Refreshes the dialog controls. This should be called after the user has resized the keyboard
* (which causes the key size to change) or if the gok data has changed.
**/
void gok_settingsdialog_refresh ()
{
	/* only the keysize/space page needs to get refreshed */
	gok_settings_page_keysizespace_refresh();
}

/**
* gok_settingsdialog_backup_settings
* 
* Copies all the member settings to backup.
**/
void gok_settingsdialog_backup_settings ()
{
	gok_settings_page_keysizespace_backup();
	gok_page_actions_backup();
	gok_page_keyboard_backup();
	gok_page_feedbacks_backup();
	gok_page_accessmethod_backup();
	gok_page_wordcomplete_backup();
	
	gok_data_backup_settings();
}

/*
 * gok_settingsdialog_combobox_sort:
 * @model: A #GtkTreeModel
 * @a: Iter pointing to a row
 * @b: Iter pointing to another row
 * @data: User data
 *
 * Alpha-numeric compare func. UTF-8.
 *
 * Returns: 0 if the items are the same, -1 if a is less than b,
 *          1 if a is greater than b.
 */
static gint
gok_settingsdialog_combobox_sort (GtkTreeModel *model,
				  GtkTreeIter  *a,
				  GtkTreeIter  *b,
				  gpointer      data)
{
	gchar *str_a, *str_b;
	gint ret = 0;

	gtk_tree_model_get (model, a, COL_TEXT, &str_a, -1);
	gtk_tree_model_get (model, b, COL_TEXT, &str_b, -1);


	if (str_a == NULL || str_b == NULL)
	{
		if (str_a == NULL && str_b == NULL)
			return 0; /* both equal */

		ret = (str_a == NULL) ? -1 : 1;
	}
	else
	{
		ret = g_utf8_collate (str_a, str_b);
	}

	g_free (str_a);
	g_free (str_b);

	return ret;
}

/**
 * gok_settingsdialog_combobox_init:
 * @combo: A #GtkComboBox
 * @sortalbe: Whether the model should be sortable
 *
 * Utility function to intitialize @combo.
 *
 * The initialization is similar to gtk_combo_box_new_text, to
 * allow the use of the 'simple text-only API' for combo boxes.
 */
void
gok_settingsdialog_combobox_init (GtkComboBox *combo,
				  gboolean     sortable)
{
	GtkListStore *model;
	GtkCellRenderer *cell;

	g_return_if_fail (GTK_IS_COMBO_BOX (combo));

	/* model */
	model = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING);
	gtk_combo_box_set_model (combo, GTK_TREE_MODEL (model));

	/* text renderer */
	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), cell, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), cell,
					"text", COL_TEXT, NULL);

	/* sorting */
	if (sortable)
	{
		gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
						 COL_TEXT,
						 gok_settingsdialog_combobox_sort,
						 NULL, NULL);
		gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
						      COL_TEXT,
						      GTK_SORT_ASCENDING);
	}
	g_object_unref (model);
}

/**
 * gok_settingsdialog_combobox_search:
 * @combo: A #GtkComboBox
 * @item: The item to search for
 * @iter_return: Return location for iter
 *
 * Searches for @item in @combo. @iter_return will point to the location of
 * @item. If the function returns #FALSE, @iter_return will be invalid.
 *
 * Returns: #TRUE if @item was found, #FALSE otherwise
 */
gboolean
gok_settingsdialog_combobox_search (GtkComboBox *combo,
				    const gchar *item,
				    GtkTreeIter *iter_return)
{
	GtkTreeModel *model;
	gchar *str;

	g_return_val_if_fail (GTK_IS_COMBO_BOX (combo), FALSE);
	g_return_val_if_fail (iter_return != NULL, FALSE);

	model = gtk_combo_box_get_model (combo);
	if (gtk_tree_model_get_iter_first (model, iter_return))
	{
		do
		{
			gtk_tree_model_get (model, iter_return, COL_TEXT, &str, -1);
			if (g_strcmp0 (item, str) == 0)
			{
				g_free (str);
				return TRUE;
			}
			g_free (str);
		}
		while (gtk_tree_model_iter_next (model, iter_return));
	}
	return FALSE;
}

/**
 * gok_settingsdialog_combobox_set_active:
 * @combo: a #GtkComboBox
 * @item: The item to show
 *
 * Set @item as the current seletction in @combo.
 */
void
gok_settingsdialog_combobox_set_active (GtkComboBox *combo,
					const gchar *item)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	g_return_if_fail (GTK_IS_COMBO_BOX (combo));
	g_return_if_fail (item != NULL);

	if (!gok_settingsdialog_combobox_search (combo, item, &iter))
	{
		/* fall back to first item */
		model = gtk_combo_box_get_model (combo);
		if (!gtk_tree_model_get_iter_first (model, &iter))
		{
			/* combo box is empty */
			return;
		}
	}
	gtk_combo_box_set_active_iter (combo, &iter);
}
