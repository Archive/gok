/*
 * gok-ui-helper.c
 *
 * Copyright 2009 Gerd Kohlberger <gerdko gmail com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gok-ui-helper.h"

/**
 * gok_ui_helper_load_file:
 * @file: Name of the .ui file 
 *
 * Loads the user interface desription from @file and
 * auto-connects all signals.
 *
 * Returns: a pointer to the newly created #GtkBuilder object, or NULL
 */
GtkBuilder *
gok_ui_helper_load_file (const gchar *file)
{
	GtkBuilder *builder;
	GError *error = NULL;
	gchar *path;

	g_return_val_if_fail (file != NULL, NULL);

	builder = gtk_builder_new ();
	path = g_build_filename (DATADIR, "gok", file, NULL);
	gtk_builder_add_from_file (builder, path, &error);
	g_free (path);

	if (error)
	{
		g_warning ("%s", error->message);
		g_error_free (error);
		g_object_unref (builder);
		return NULL;
	}

	gtk_builder_connect_signals (builder, NULL);

	return builder;
}

/**
 * gok_ui_helper_get_object:
 * @builder: a #GtkBuilder object
 * @name: name of the object
 * @file: ouput of __FILE__
 * @line: ouput of __LINE__
 * @func: ouput of G_STRFUNC
 *
 * Gets the object with @name from @builder. This function
 * should only be accessed through the OID() and WID() macros.
 *
 * Returns: a #GObject, or NULL
 */
GObject *
gok_ui_helper_get_object (GtkBuilder  *builder,
			  const gchar *name,
			  const gchar *file,
			  gint         line,
			  const gchar *func)
{
	GObject *object;

	object = gtk_builder_get_object (builder, name);
	/* we can't use g_assert here, because
	 * it would always report this location.
	 */
#ifndef G_DISABLE_ASSERT
	if (G_UNLIKELY (object == NULL))
	{
		g_assertion_message_expr (G_LOG_DOMAIN,
					  file, line, func,
					  "object != NULL");
	}
#endif
	return object;
}
