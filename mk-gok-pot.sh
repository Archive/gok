#!/bin/sh

# This script builds a gettext po template for gok.
# The template can be used to build a po file of
# translated strings for a particular language.
#
# This script runs xgettext on all the files listed in po/POTFILES.in
# and builds the template file po/gok.pot

xgettext --files-from=po/POTFILES.in  \
         --keyword=_                  \
         --output-dir=po              \
         --output=gok.pot
