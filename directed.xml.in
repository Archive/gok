<?xml version="1.0"?>
<GOK:accessmethodroot xmlns:GOK="http://www.gnome.org/GOK">

<!-- User interface for the access method -->
<GOK:accessmethod name="directed" _displayname="5 Switch Directed">
<_GOK:description xml:space="preserve">Move the highlighter in 4 directions.  Select key.</_GOK:description>

<GOK:operation>
<GOK:control type="frame" _string="Operation">
	<GOK:control type="vbox" spacing="4" border="12">
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Up:" />
			<GOK:control name="moveup" _string="Up:" type="combobox" fillwith="actions" qualifier="switch+mousebutton" />
		</GOK:control>
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Down:" />
			<GOK:control name="movedown" _string="Down:" type="combobox" fillwith="actions" qualifier="switch+mousebutton" />
		</GOK:control>
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Left:" />
			<GOK:control name="moveleft" _string="Left:" type="combobox" fillwith="actions" qualifier="switch+mousebutton" />
		</GOK:control>
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Right:" />
			<GOK:control name="moveright" _string="Right:" type="combobox" fillwith="actions" qualifier="switch+mousebutton" />
		</GOK:control>
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Activate:" />
			<GOK:control name="select" type="combobox" _string="Select:" fillwith="actions" qualifier="switch+mousebutton" />
		</GOK:control>
	</GOK:control>
</GOK:control>
</GOK:operation>

<GOK:feedback>
<GOK:control type="frame" _string="Feedback">
	<GOK:control type="vbox" spacing="4" border="12">
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Movement:" />
			<GOK:control name="feedbackmovement" _string="Feedback Movement:" type="combobox" fillwith="feedbacks" />
		</GOK:control>
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Activation:" />
			<GOK:control name="feedbackselection" _string="Feedback Movement:" type="combobox" fillwith="feedbacks" />
		</GOK:control>
	</GOK:control>
</GOK:control>
</GOK:feedback>

<GOK:options>
<GOK:control type="frame" _string="Options">
	<GOK:control type="vbox" spacing="4" border="12">
		<GOK:control type="hbox" spacing="12">
			<GOK:control type="label" _string="Scan Delay:" />
			<GOK:control name="scanspeed" type="spinbutton" _string="Scan Speed:" value="100" min="10" max="500" stepincrement="1" pageincrement="10" pagesize="0" />
			<GOK:control type="label" _string="(100s of a second)" />
		</GOK:control>
		<GOK:control name="wrap" type="checkbutton" _string="Wrap Rows/Columns" />
	</GOK:control>
</GOK:control>
</GOK:options>

<!-- Rates for this access method. -->
	<GOK:rate name="moveleft" stringvalue="switch1" />
	<GOK:rate name="moveright" stringvalue="switch2" />
	<GOK:rate name="moveup" stringvalue="switch3" />
	<GOK:rate name="movedown" stringvalue="switch4" />
	<GOK:rate name="select" stringvalue="switch5" />
	<GOK:rate name="feedbackmovement" stringvalue="none" />
	<GOK:rate name="feedbackselection" stringvalue="key_flashing" />
	<GOK:rate name="scanspeed" value="100" />
	<GOK:rate name="wrap" value="1" />
	<GOK:rate name="topbottom" value="0" />
	<GOK:rate name="leftright" value="0" />
	<GOK:rate name="cycles" value="3" />

	<GOK:rate name="autorepeat" value="0" />
	<GOK:rate name="repeatrate" value="30" />

<!-- Initialization for the access method. -->
	<GOK:initialization>
		<GOK:effect call="ChunkerReset" />
		<GOK:effect call="ChunkerChunkKeys" param1="topbottom" param2="leftright" />
	</GOK:initialization>

<!-- Scan the chunks (probably keys) -->
	<GOK:state name="1">

<!-- Right -->
		<GOK:handler name="moveright" state="press">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer1Set" name="scanspeed" />
			<GOK:effect call="ChunkerIfRight">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyRight" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToLeft" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

<!-- Left -->
		<GOK:handler name="moveleft" state="press">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer2Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfLeft">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyLeft" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToRight" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

<!-- Up -->
		<GOK:handler name="moveup" state="press">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer3Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfTop">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyUp" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToBottom" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

<!-- Down -->
		<GOK:handler name="movedown" state="press">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer4Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfBottom">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyDown" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToTop" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

<!-- release timers -->
		<GOK:handler name="moveright" state="release">
			<GOK:effect call="Timer1Stop" />
		</GOK:handler>
		<GOK:handler name="moveleft" state="release">
			<GOK:effect call="Timer2Stop" />
		</GOK:handler>
		<GOK:handler name="moveup" state="release">
			<GOK:effect call="Timer3Stop" />
		</GOK:handler>
		<GOK:handler name="movedown" state="release">
			<GOK:effect call="Timer4Stop" />
		</GOK:handler>


<!-- Select -->
		<GOK:handler name="select">
			<GOK:effect call="Timer1Stop" />
			<GOK:effect call="Timer2Stop" />
			<GOK:effect call="Timer3Stop" />
			<GOK:effect call="Timer4Stop" />
			<GOK:effect call="SelectChunk" >
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="Feedback" name="feedbackselection" />
						<GOK:effect call="OutputSelectedKey" />
						<GOK:effect call="HighlightCenterKey"/>
						<GOK:effect call="StateJump" param1="1" />
					</GOK:true>
<!--					<GOK:false>
						<GOK:effect call="HighlightFirstKey" />
						<GOK:effect call="StateJump" param1="2" />
					</GOK:false> -->
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>


<!-- Scanning -->

		<!-- Scan Right is same as moveright handler -->
		<GOK:handler name="timer1">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer1Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfNextChunk">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerNextChunk" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap">
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapFirstChunk" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

		<!-- Scan Left is same as moveleft handler -->
		<GOK:handler name="timer2">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer2Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfLeft">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyLeft" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToRight" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

		<!-- Scan Up is same as moveup handler -->
		<GOK:handler name="timer3">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer3Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfTop">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyUp" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToBottom" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>

		<!-- Scan Down is same as movedown handler -->
		<GOK:handler name="timer4">
			<GOK:effect call="Feedback" name="feedbackmovement" />
			<GOK:effect call="Timer4Set" name="scanspeed" /> 
			<GOK:effect call="ChunkerIfBottom">
				<GOK:compare type="equal" value="1">
					<GOK:true>
						<GOK:effect call="ChunkerKeyDown" />
					</GOK:true>
					<GOK:false>
						<GOK:effect call="GetRate" name="wrap" >
							<GOK:compare type="equal" value="1">
								<GOK:true>
									<GOK:effect call="ChunkerWrapToTop" />
								</GOK:true>
							</GOK:compare>
						</GOK:effect>
					</GOK:false>
				</GOK:compare>
			</GOK:effect>
		</GOK:handler>
		
	</GOK:state>


<!-- For repeat state choose a handler for turning repeat off. -->
	<GOK:state name="repeat">
		<GOK:handler name="select">
			<GOK:effect call="StateReset" name="repeat:state-reset" />
			<GOK:effect call="UnhighlightAll" name="repeat:unhighlight-all" />
			<GOK:effect call="Timer1Stop" />
			<GOK:effect call="Timer2Stop" />
			<GOK:effect call="Timer3Stop" />
			<GOK:effect call="Timer4Stop" />
			<GOK:effect call="HighlightFirstChunk" />
			<GOK:effect call="StateJump" param1="1" />
		</GOK:handler>
	</GOK:state>


</GOK:accessmethod>
</GOK:accessmethodroot>
